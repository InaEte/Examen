package com.ucbcba.proyecto.proyecto.Entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name="Zona")
public class Zona {
    @Id
    @GeneratedValue
    private int idZona;

    @NotNull
    private String nombre;

   // @OneToMany(mappedBy = "zona")
    //private Set<Pedido> pedidos;

    public int getId() {
        return idZona;
    }

    public void setId(int idZona) {
        this.idZona = idZona;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /*public Set<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(Set<Pedido> pedidos) {
        this.pedidos = pedidos;
    }
    */
}
